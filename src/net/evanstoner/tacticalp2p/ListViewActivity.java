package net.evanstoner.tacticalp2p;

import java.util.Date;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import net.evanstoner.tacticalp2p.R.id;
import android.location.Location;
import android.os.Bundle;
import android.app.Activity;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.widget.SimpleCursorAdapter;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ListViewActivity extends FragmentActivity implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {

	ListView lstMarkers;
	
	private static final int MARKER_LOADER = 0;
	
	private TP2PSQLHelper mDBHelper;
	private MarkerDetailAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_list_view);
	    
	    lstMarkers = (ListView)findViewById(R.id.lstMarkers);
	    
	    mDBHelper = new TP2PSQLHelper(this);
	    mAdapter = new MarkerDetailAdapter(this, null, true);
	    lstMarkers.setAdapter(mAdapter);
	    lstMarkers.setOnItemClickListener(new OnItemClickListener() {
	    	@Override
	    	public void onItemClick(AdapterView<?> parent, android.view.View view, int position, long id) {
	    		UUID markerID = (UUID)view.getTag();
	    		Intent intent = new Intent(HomeActivity.getContext(), MarkerDetailsActivity.class);
	    		intent.putExtra(MarkerDetailsActivity.EXTRA_MARKER_ID, markerID);
	    		startActivity(intent);
	    	}
	    });

	    getSupportLoaderManager().initLoader(MARKER_LOADER, null, this);
	}
	
	/*
	* Callback that's invoked when the system has initialized the Loader and
	* is ready to start the query. This usually happens when initLoader() is
	* called. The loaderID argument contains the ID value passed to the
	* initLoader() call.
	*/
	@Override
	public android.support.v4.content.Loader<Cursor> onCreateLoader(int loaderID, Bundle bundle)
	{
	    /*
	     * Takes action based on the ID of the Loader that's being created
	     */
	    switch (loaderID) {
	        case MARKER_LOADER:
	            // Returns a new CursorLoader
	            return new MarkerLoader(this, mDBHelper);
	        default:
	            // An invalid id was passed in
	            return null;
	    }
	}
	
	@Override
	public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
    }

	@Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
            mAdapter.swapCursor(null);
    }

}
