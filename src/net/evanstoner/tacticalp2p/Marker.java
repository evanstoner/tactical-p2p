package net.evanstoner.tacticalp2p;
import java.util.Date;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import android.location.Location;


	public class Marker {

        public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public String getShortDescription() {
			return shortDescription;
		}

		public void setShortDescription(String shrotDescription) {
			this.shortDescription = shrotDescription;
		}

		public String getLongDescription() {
			return longDescription;
		}

		public void setLongDescription(String longDescription) {
			this.longDescription = longDescription;
		}

		public UUID getAuthor() {
			return author;
		}

		public void setAuthor(UUID author) {
			this.author = author;
		}

		public Date getTime() {
			return time;
		}

		public void setTime(Date time) {
			this.time = time;
		}
		
        public Date getExpiration() {
			return expiration;
		}

		public void setExpiration(Date expiration) {
			this.expiration = expiration;
		}

		public int getRadius() {
			return radius;
		}

		public void setRadius(int radius) {
			this.radius = radius;
		}

		public Queue<Location> getLocation() {
			return location;
		}
		
		public String getLocationString() {
			String str = "";
			for (Location l : location) {
				str += l.getLatitude() + "," + l.getLongitude() + ";";
			}
			return str;
		}

		public void setLocation(Queue<Location> location) {
			this.location = location;
		}
        
        public UUID getId() {
			return id;
		}

		public void setId(UUID id) {
			this.id = id;
		}

        private int type;
		private String shortDescription;
        private String longDescription;
        private UUID author;
        private Date time;
        private Date expiration;
		private int radius;
        private Queue<Location> location;
        private UUID id;


		public Marker(){
			time = new Date();
            id = UUID.randomUUID();     
            location = new ArrayBlockingQueue<Location>(10);
            int expirationMod = ScenarioDefinition.getCurrent(HomeActivity.getContext()).getDefaultExpirationTime();
            expiration = new Date( time.getTime() + 1000 * expirationMod);
            type = ScenarioDefinition.getCurrent(HomeActivity.getContext()).getDefaultMarkerType().getID();
            author = DeviceSettings.getAuthorID(HomeActivity.getContext());
		}

        public Marker(final int newType, final String newShortDescription,final String newLongDescription,
        		final UUID newAuthor,final Date newTime, final Date newExpiration, final int newRadius,
        		final Queue<Location> newLocation, UUID newID) {
        	type = newType;
            shortDescription = newShortDescription;
            longDescription = newLongDescription;
            author = newAuthor;
            time = newTime;
            expiration = newExpiration;
            radius = newRadius;
            location = newLocation;
            id = newID;
        }
        
        public static Queue<Location> LocationStringToQueue(String locationString) {
        	String[] locations = locationString.split(";");
        	Queue<Location> locationQ = new ArrayBlockingQueue<Location>(10);
        	for (int i = 0; i < locations.length; i++) {
        		String[] latLong = locations[i].split(",");
        		Location location = new Location("record");
        		try {
        			location.setLatitude(Double.parseDouble(latLong[0]));
        			location.setLongitude(Double.parseDouble(latLong[1]));
        			locationQ.add(location);
        		} catch (Exception ex) {
        			// ignore this record
        		}
        	}
        	return locationQ;
        }
}

