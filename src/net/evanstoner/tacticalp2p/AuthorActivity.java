package net.evanstoner.tacticalp2p;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class AuthorActivity extends FragmentActivity implements MarkerTypePickerDialogFragment.MarkerTypePickerListener {
	
	TextView txtMarkerID;
	EditText edtShortDesc;
	TextView txtType;
	ImageView imgTypeIcon;
	EditText edtLongDesc;
	EditText edtRadius;
	DatePicker dateExpiration;
	
	Marker marker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_author);
		
		txtMarkerID = (TextView)findViewById(R.id.txtMarkerID);
		edtShortDesc = (EditText)findViewById(R.id.edtShortDesc);
		txtType = (TextView)findViewById(R.id.txtType);
		imgTypeIcon = (ImageView)findViewById(R.id.imgTypeIcon);
		edtLongDesc = (EditText)findViewById(R.id.edtLongDesc);
		edtRadius = (EditText)findViewById(R.id.edtRadius);
		dateExpiration = (DatePicker)findViewById(R.id.dateExpiration);
		
		marker = new Marker();
		
		// set expiration date
		Calendar cal = Calendar.getInstance();
		cal.setTime(marker.getExpiration());
		dateExpiration.updateDate(
				cal.get(cal.YEAR), 
				cal.get(cal.MONTH), 
				cal.get(cal.DAY_OF_MONTH));

		// show UUID of new marker
		txtMarkerID.setText(marker.getId().toString());
		
		// show marker type
		MarkerType defaultMarkerType = ScenarioDefinition.getCurrent(HomeActivity.getContext()).getDefaultMarkerType();
		marker.setType(defaultMarkerType.getID());
		onMarkerTypeSelected(defaultMarkerType.getID(), defaultMarkerType.getName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.author, menu);
		return true;
	}
	
	public void btnChangeType_click(View view) {
		MarkerTypePickerDialogFragment typePicker = new MarkerTypePickerDialogFragment();
		typePicker.show(getSupportFragmentManager(), "typePicker");
	}
	
	public void btnSaveMarker_click(View view) {
		marker.setShortDescription(edtShortDesc.getText().toString());
		marker.setLongDescription(edtLongDesc.getText().toString());
		String radius = edtRadius.getText().toString();
		if (radius.trim().equals("")) {
			radius = "0";
		}
		marker.setRadius(Integer.parseInt(radius));
		TP2PSQLHelper dbHelper = new TP2PSQLHelper(this);
		dbHelper.addMarker(marker);
		Toast.makeText(this, "Marker saved.", Toast.LENGTH_SHORT).show();
		finish();
	}

	@Override
	public void onMarkerTypeSelected(int id, String name) {
		marker.setType(id);
		txtType.setText(name);
		imgTypeIcon.setImageDrawable(ScenarioDefinition.getCurrent(this).getDrawableByMarkerType(getResources(), id));
	}

}
