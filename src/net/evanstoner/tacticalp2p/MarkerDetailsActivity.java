package net.evanstoner.tacticalp2p;

import java.text.SimpleDateFormat;
import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MarkerDetailsActivity extends Activity {
	
	public static String EXTRA_MARKER_ID = "com.evanstoner.tacticalp2p.MARKER_ID";
	
	TextView txtShortDesc;
	TextView txtLongDesc;
	TextView txtRadius;
	TextView txtAuthor;
	TextView txtTime;
	TextView txtExpiration;
	TextView txtLocation;
	TextView txtID;
	TextView txtType;
	ImageView imgTypeIcon;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_marker_details);
		
		txtShortDesc = (TextView)findViewById(R.id.txtDetailsShortDesc);
		txtLongDesc = (TextView)findViewById(R.id.txtDetailsLongDesc);
		txtRadius = (TextView)findViewById(R.id.txtDetailsRadius);
		txtAuthor = (TextView)findViewById(R.id.txtDetailsAuthor);
		txtTime = (TextView)findViewById(R.id.txtDetailsTime);
		txtExpiration = (TextView)findViewById(R.id.txtDetailsExpiration);
		txtLocation = (TextView)findViewById(R.id.txtDetailsLocation);
		txtID = (TextView)findViewById(R.id.txtDetailsID);
		txtType = (TextView)findViewById(R.id.txtDetailsType);
		imgTypeIcon = (ImageView)findViewById(R.id.imgDetailsTypeIcon);
		
		TP2PSQLHelper dbHelper = new TP2PSQLHelper(this);
		UUID id = (UUID)(getIntent().getSerializableExtra(EXTRA_MARKER_ID));
		Marker marker = dbHelper.getMarkerByID(id);
		if (marker == null) {
			Toast.makeText(this, "Invalid marker ID", Toast.LENGTH_SHORT).show();
			finish();
		}
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		txtShortDesc.setText(marker.getShortDescription());
		txtLongDesc.setText(marker.getLongDescription());
		txtRadius.setText("" + marker.getRadius());
		txtAuthor.setText(marker.getAuthor().toString());
		txtTime.setText(df.format(marker.getTime()));
		txtExpiration.setText(df.format(marker.getExpiration()));
		txtLocation.setText(marker.getLocationString()); // TODO make this pretty
		txtID.setText(marker.getId().toString());
		txtType.setText("" + marker.getType()); // TODO get marker type name
		imgTypeIcon.setImageDrawable(ScenarioDefinition.getCurrent(this).getDrawableByMarkerType(getResources(), marker.getType()));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.marker_details, menu);
		return true;
	}

}
