package net.evanstoner.tacticalp2p;

import java.util.UUID;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class CreateMissionActivity extends Activity {
	
	private UUID gID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_mission);
		gID = UUID.randomUUID();
		TextView txtID = (TextView)findViewById(R.id.txtID);
		txtID.setText(gID.toString());
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.create_mission, menu);
		return true;
	}

}
