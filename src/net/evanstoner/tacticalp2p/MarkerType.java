package net.evanstoner.tacticalp2p;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class MarkerType {
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDrawableName() {
		return drawableName;
	}
	public void setDrawableName(String drawableName) {
		this.drawableName = drawableName;
	}
	
	/**
	 * 
	 * @param resource your resource object. In an activity, it comes from the getResource() call
	 * @return a drawable image
	 */
	public Drawable getImage(Resources resource) {
		if (image == null){
			
			try {
				int id= resource.getIdentifier(drawableName, "drawable", "net.evanstoner.tacticalp2p");
				
				image = resource.getDrawable(id);
				
			} catch (Exception e){
				image = resource.getDrawable(R.drawable.question);
			}
			
		}
		return image;
	}

	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	private String name;
	private String drawableName;
	private Drawable image;
	private int ID;
	private int priority;
	

}
