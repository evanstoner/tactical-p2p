package net.evanstoner.tacticalp2p;

import java.util.Date;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

public class TP2PSQLHelper extends SQLiteOpenHelper {

	private static final String DATABASE_NAME = "tacticalp2p.db";
	private static final int DATABASE_VERSION = 1;
	
	public static final String MARKERS = "markersTable";
	public static final String MARKERS_ID = "_id";
	public static final String MARKERS_TYPE = "type";
	public static final String MARKERS_SHORT_DESC = "shortDescription";
	public static final String MARKERS_LONG_DESC = "longDescription";
	public static final String MARKERS_AUTHOR = "author";
	public static final String MARKERS_TIME = "time";
	public static final String MARKERS_EXPIRATION = "expiration";
	public static final String MARKERS_RADIUS = "radius";
	public static final String MARKERS_LOCATION = "location";
	
	private static final String DATABASE_CREATE = 
		"create table " + MARKERS
			+ "("
			+ MARKERS_ID + " text,"
			+ MARKERS_TYPE + " integer,"
			+ MARKERS_SHORT_DESC + " text,"
			+ MARKERS_LONG_DESC + " text,"
			+ MARKERS_AUTHOR + " text,"
			+ MARKERS_TIME + " integer,"
			+ MARKERS_EXPIRATION + " integer,"
			+ MARKERS_RADIUS + " integer,"
			+ MARKERS_LOCATION + " text"
			+ ");";
	
	private static final String DATABASE_DROP = 
		"drop table if exists " + MARKERS;
	
	public TP2PSQLHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DATABASE_DROP);
	    onCreate(db);
	}
	
	public void addMarker(Marker marker) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(MARKERS_ID, marker.getId().toString());
		cv.put(MARKERS_TYPE, marker.getType());
		cv.put(MARKERS_SHORT_DESC, marker.getShortDescription());
		cv.put(MARKERS_LONG_DESC, marker.getLongDescription());
		cv.put(MARKERS_AUTHOR, marker.getAuthor().toString());
		cv.put(MARKERS_TIME, marker.getTime().getTime());
		cv.put(MARKERS_EXPIRATION, marker.getExpiration().getTime());
		cv.put(MARKERS_RADIUS, marker.getRadius());
		cv.put(MARKERS_LOCATION, marker.getLocationString());
		db.insert(MARKERS, null, cv);
		db.close();
	}
	
	public Cursor getAllMarkers() {
		SQLiteDatabase db = this.getReadableDatabase();
		return db.query(
			MARKERS, // table
			new String[] {"*"}, // columns
			null, // where statement 
			null, // where args
			null, // group by
			null, // having
			null // order by
			);
	}
	
	public Marker getMarkerByID(UUID id) {
		if (id == null) {
			return null;
		}
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(
			MARKERS, // table
			new String[] {"*"}, // columns
			MARKERS_ID + "=?", // where statement 
			new String[] {id.toString()}, // where args
			null, // group by
			null, // having
			null // order by
			);
		
		if (cursor.getCount() == 0) {
			return null;
		}
		
		cursor.moveToFirst();
		
		int type = cursor.getInt(cursor.getColumnIndex(MARKERS_TYPE));
		String shortDesc = cursor.getString(cursor.getColumnIndex(MARKERS_SHORT_DESC));
		String longDesc = cursor.getString(cursor.getColumnIndex(MARKERS_LONG_DESC));
		UUID author = UUID.fromString(cursor.getString(cursor.getColumnIndex(MARKERS_AUTHOR)));
		Date time = new Date(cursor.getLong(cursor.getColumnIndex(MARKERS_TIME)));
		Date expiration = new Date(cursor.getLong(cursor.getColumnIndex(MARKERS_EXPIRATION)));
		int radius = cursor.getInt(cursor.getColumnIndex(MARKERS_RADIUS));
		String locationString = cursor.getString(cursor.getColumnIndex(MARKERS_LOCATION));		
		
		Marker marker = new Marker(
				type,
				shortDesc,
				longDesc,
				author,
				time,
				expiration,
				radius,
				Marker.LocationStringToQueue(locationString),
				id
				);
		
		return marker;
	}

}
