package net.evanstoner.tacticalp2p;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import com.google.gson.Gson;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.util.Log;

public class ScenarioDefinition {
	private transient static ScenarioDefinition def;
	private String name;
	private int userGPSTrackCount;
	private int defaultExpirationTime;
	private MarkerType[] markers;
	private int defaultMarkerID;
	
	public int getDefaultMarkerID() {
		return defaultMarkerID;
	}
	/**
	 * gets the default marker type
	 * @return the default marker type
	 */
	public MarkerType getDefaultMarkerType(){
		for (MarkerType m : markers)
			if (m.getID() == defaultMarkerID)
				return m;
	 return markers[0];
	}

	public void setDefaultMarkerID(int defaultMarkerID) {
		this.defaultMarkerID = defaultMarkerID;
	}

	public int getUserGPSTrackCount() {
		return userGPSTrackCount;
	}

	public void setUserGPSTrackCount(int userGPSTrackCount) {
		this.userGPSTrackCount = userGPSTrackCount;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MarkerType[] getMarkers() {
		return markers;
	}

	public void setMarkers(MarkerType[] markers) {
		this.markers = markers;
	}
	
	public int getDefaultExpirationTime() {
		return defaultExpirationTime;
	}

	public void setDefaultExpirationTime(int defaultExpirationTime) {
		this.defaultExpirationTime = defaultExpirationTime;
	}
	//returns this object as a json string
	public String toJSON(){
		Gson gson = new Gson();
		
		return gson.toJson(this);
		
	}
	
	public static ScenarioDefinition fromJSON(String jsonRepresentation){
		Gson gson = new Gson();
		return gson.fromJson(jsonRepresentation, ScenarioDefinition.class);
	}

	public MarkerType getMarkerTypeByID(int id){
		for (MarkerType mt : markers)
			if (mt.getID() == id)
				return mt;
		return null;
	}
	/**
	 * a rough method do dump an example scenario into the shared preferences cache
	 * @param c
	 */	public static void dumpExampleToSharedPreferences(Context c){
		//read in our reference file for now.
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
		InputStream is = c.getResources().openRawResource(R.raw.examplescenario);
		InputStreamReader inputreader = new InputStreamReader(is);
		
		//convert it to a string
        BufferedReader buffreader = new BufferedReader(inputreader);
         String line;
         StringBuilder text = new StringBuilder();

         try {
           while (( line = buffreader.readLine()) != null) {
               text.append(line);
               text.append('\n');
             }
       } catch (IOException e) {
    	   Log.e("io-exception", e.toString());
       }
		
         //uhhh... instantiate. Lame but workable. Need the name of the scenario for writing to preferences
         ScenarioDefinition sd = ScenarioDefinition.fromJSON(text.toString());
         //write to preferences!
		Editor prefsEditor = prefs.edit();
		prefsEditor.putString(sd.getName(), text.toString());
		
		//also stick the active scenario name somewhere where we can retrieve it, to be more conducent to multiple scenarios at once.
		prefsEditor.putString("active_scenario", sd.getName());
		prefsEditor.commit();
		
	}

	/**
	 * pulls the first "current" scenario definition from shared preferences
	 * @param c the context (getApplicationContext() returns this object in an activity)
	 * @return an object representing hte currently loaded scenario definition
	 */
	public static ScenarioDefinition getCurrent( Context c){
		SharedPreferences sharedprefs = PreferenceManager.getDefaultSharedPreferences(c);
		String key = sharedprefs.getString("active_scenario", "");
		
		if ( def == null || !key.equals(def.name)){

			if (!key.equals("")){
				def = ScenarioDefinition.fromJSON(sharedprefs.getString(key, "")) ;
			}
		}
		return def;
	}
	
	/**
	 * Maps the int value of ID from marker type to a Drawable Image
	 * @param r a resources object (getResources returns this)
	 * @param markerTypeID an int, the ID of the MarkerType to look up
	 * @return a Drawable that is the icon of this type of marker
	 */
	public Drawable getDrawableByMarkerType(Resources r, int markerTypeID){
		for (MarkerType m : markers){
			if (m.getID() == markerTypeID){
				return m.getImage(r);				
			}
		}
		
		//return the default value if it's an unrecognized type.
		return new MarkerType().getImage(r);
	}
}
