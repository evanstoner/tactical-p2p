package net.evanstoner.tacticalp2p;

import android.content.Context;
import android.database.Cursor;

public class MarkerLoader extends SimpleCursorLoader {
	
	private TP2PSQLHelper mDBHelper;
	
	public MarkerLoader(Context context, TP2PSQLHelper dbHelper) {
		super(context);
		mDBHelper = dbHelper;
	}

	@Override
	public Cursor loadInBackground() {
		return mDBHelper.getAllMarkers();
	}

}
